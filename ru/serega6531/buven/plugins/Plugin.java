package ru.serega6531.buven.plugins;

public abstract class Plugin {

    private String version;

    public Plugin(String version){
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public abstract String getLink();

}
