package ru.serega6531.buven.plugins;

public class SpigotMCPlugin extends Plugin {

    private int id;

    public SpigotMCPlugin(String version, int id) {
        super(version);
        this.id = id;
    }

    @Override
    public String getLink() {
        return "https://www.spigotmc.org/resources/" + id + "/download?version=" + getVersion();
    }
}
