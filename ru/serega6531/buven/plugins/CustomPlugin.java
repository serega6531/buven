package ru.serega6531.buven.plugins;

public class CustomPlugin extends Plugin {

    private String link;

    public CustomPlugin(String version, String link) {
        super(version);
        this.link = link;
    }

    public String getLink() {
        return link;
    }

}
