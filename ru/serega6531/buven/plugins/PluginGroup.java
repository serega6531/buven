package ru.serega6531.buven.plugins;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PluginGroup {

    private Map<String, Plugin> plugins = new HashMap<>();

    public PluginGroup(String name, JSONObject json){
        for(Object k : json.keySet()){
            String version = (String)k;
            JSONObject obj = (JSONObject)json.get(version);
            if(obj.containsKey("link"))
                plugins.put(version, new CustomPlugin(version, (String) obj.get("link")));
            else
                plugins.put(version, new SpigotMCPlugin(version, (int)obj.get("spigotid")));
        }
    }

    public Plugin get(String version){
        return plugins.get(version);
    }

}
