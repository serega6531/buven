package ru.serega6531.buven;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Buven {

    private static PluginsList plugins;

    public static void main(String[] args) throws IOException {
        File confd = new File(".buven");
        if(confd.exists() && !confd.isDirectory()){
            System.err.println("Error: .buven exists and its not a directory");
            return;
        }

        File conff = new File(confd, "pluginspath.conf");
        File pluginsf = new File(confd, "plugins.json");

        if(!conff.exists() || (args.length > 0 && args[0].equals("setplugins"))){
            System.out.println("Enter plugins config url:");
            conff.createNewFile();
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            String url = read.readLine();
            read.close();

            FileWriter writer = new FileWriter(conff);
            writer.write(url);
            writer.close();

            if(!updatePlugins(url, pluginsf))
                return;
        }

        if(args.length > 0 && args[0].equals("updateplugins")){
            BufferedReader read = new BufferedReader(new FileReader(conff));
            String url = read.readLine();
            read.close();
            if(!updatePlugins(url, pluginsf))
                return;
        }

        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader(pluginsf));
            plugins = new PluginsList(obj);
        } catch (ParseException e) {
            System.err.println("Error parsing json: " + e.toString());
            return;
        }


        if(args.length == 0){
            System.err.println("You should specify makefile");
        }
    }

    private static boolean updatePlugins(String from, File to){
        System.out.println("Attempting to download plugins info");
        try {
            downloadFile(from, to);
        } catch (MalformedURLException e){
            System.err.println("Cant parse url. Maybe protocol is missing? (" + e.getMessage() + ")");
            return false;
        } catch (IOException e){
            System.err.println("Cant download file. Check url. (" + e.getLocalizedMessage() + ")");
            return false;
        }

        return true;
    }

    private static void downloadFile(String from, File to) throws MalformedURLException, IOException {
        URL url = new URL(from);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(to);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }

}
