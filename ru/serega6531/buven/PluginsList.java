package ru.serega6531.buven;

import org.json.simple.JSONObject;
import ru.serega6531.buven.plugins.Plugin;
import ru.serega6531.buven.plugins.PluginGroup;

import java.util.HashMap;
import java.util.Map;

public class PluginsList {

    private Map<String, PluginGroup> groups = new HashMap<>();

    public PluginsList(JSONObject json){
        for(Object k : json.keySet()){
            String name = (String)k;
            loadPluginGroup(name, (JSONObject)json.get(k));
        }
    }

    private void loadPluginGroup(String name, JSONObject json){
        PluginGroup group = new PluginGroup(name, json);
        groups.put(name, group);
    }

    public Plugin getPlugin(String name, String version){
        PluginGroup group = groups.get(name);
        if(group == null) return null;
        return group.get(version);
    }

}
